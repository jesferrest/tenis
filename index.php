<?php

include('class/tenis.php');

$partido = new Tenis();

while(!$partido->final_match()){
    if($partido->is_new_set()){
        echo '<br /><b>SETS:</b> '.$partido->get_sets(1).' - '.$partido->get_sets(2).'<br />';
    }
    echo 'POINTS: '.$partido->get_points(1).' - '.$partido->get_points(2).'<br />';
    if($partido->deuce()){
        echo '<b>DEUCE</b><br />';
    }
    
    $partido->set_point(rand(1,2));
}

echo '<br /><b>FINAL MATCH:</b> '.$partido->get_sets(1).' - '.$partido->get_sets(2).'<br />';
