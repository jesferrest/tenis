<?php

require_once '../class/tenis.php';
 
class TenisTest extends PHPUnit_Framework_TestCase {
 
    public function testGetPoints() {
        $partido = new Tenis();
        $this->assertEquals(0, $partido->get_points(1));
        $this->assertEquals(0, $partido->get_points(2));
    }
 
    public function testGetSets() {
        $partido = new Tenis();
        $this->assertEquals(0, $partido->get_sets(1));
        $this->assertEquals(0, $partido->get_sets(2));
    }
 
    public function testSetPoint() {
        $partido = new Tenis();
        $partido->set_point(1);
        $this->assertEquals(15, $partido->get_points(1));
        $this->assertEquals(0, $partido->get_points(2));
        $partido->set_point(2);
        $this->assertEquals(15, $partido->get_points(1));
        $this->assertEquals(15, $partido->get_points(2));
        $partido->set_point(1);
        $this->assertEquals(30, $partido->get_points(1));
        $this->assertEquals(15, $partido->get_points(2));
        $partido->set_point(1);
        $this->assertEquals(40, $partido->get_points(1));
        $this->assertEquals(15, $partido->get_points(2));
        $partido->set_point(1);
        $this->assertEquals(0, $partido->get_points(1));
        $this->assertEquals(0, $partido->get_points(2));
        $this->assertEquals(1, $partido->get_sets(1));
        $this->assertEquals(0, $partido->get_sets(2));
    }
 
    public function testSetWithDeuce() {
        $partido = new Tenis();
        $this->assertEquals(false, $partido->deuce());
        $partido->set_point(1);
        $partido->set_point(1);
        $partido->set_point(1);
        $partido->set_point(2);
        $partido->set_point(2);
        $partido->set_point(2);
        $this->assertEquals(true, $partido->deuce());
        $partido->set_point(1);
        $this->assertEquals('ADV', $partido->get_points(1));
        $this->assertEquals(false, $partido->deuce());
        $partido->set_point(2);
        $this->assertEquals(40, $partido->get_points(1));
        $this->assertEquals(40, $partido->get_points(2));
        $this->assertEquals(true, $partido->deuce());
        $partido->set_point(2);
        $this->assertEquals('ADV', $partido->get_points(2));
        $this->assertEquals(false, $partido->deuce());
        $partido->set_point(2);
        $this->assertEquals(0, $partido->get_points(1));
        $this->assertEquals(0, $partido->get_points(2));
        $this->assertEquals(0, $partido->get_sets(1));
        $this->assertEquals(1, $partido->get_sets(2));
    }
    
    public function testIsNewSet(){
        $partido = new Tenis();
        $this->assertEquals(true, $partido->is_new_set());
        $partido->set_point(1);
        $this->assertEquals(false, $partido->is_new_set());
        $partido->set_point(1);
        $partido->set_point(1);
        $partido->set_point(1);
        $this->assertEquals(true, $partido->is_new_set());
    }
    
    public function testFinalMatch(){
        $partido = new Tenis();
        $this->assertEquals(false, $partido->final_match());
        for($i = 0; $i < 12; $i++){
            $partido->set_point(1);
        }
        $this->assertEquals(true, $partido->final_match());
    }
}