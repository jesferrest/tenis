<?php

class Tenis{
    
    private $points = array('0', '15', '30', '40', 'ADV');
    private $max_sets = 3;

    private $sets_p1;
    private $sets_p2;
    private $points_p1;
    private $points_p2;
    
    public function __construct() {
        $this->sets_p1 = 0;
        $this->sets_p2 = 0;
        $this->points_p1 = 0;
        $this->points_p2 = 0;
    }
    
    public function get_points($player){
        if(1 == $player){
            return $this->points[$this->points_p1];
        }
        return $this->points[$this->points_p2];
    }
    
    public function get_sets($player){
        if(1 == $player){
            return $this->sets_p1;
        }
        return $this->sets_p2;
    }
    
    public function set_point($player){
        if(!$this->final_match()){
            //primero miramos si vienen de jugar deuce ya que de ser asi hay que hacer un punto mas del ADV
            if($this->post_deuce()){
                if((1 == $player && $this->points_p1 == 4) ||
                    (2 == $player && $this->points_p2 == 4)){
                    $this->set_set($player);
                }
                else {
                    $this->points_p1 = 3;
                    $this->points_p2 = 3;
                }
            }
            else{
                if(1 == $player){
                    $this->points_p1++;
                    if($this->points_p1 == 4 && !$this->post_deuce()){
                        $this->set_set($player);
                    }
                }
                else {
                    $this->points_p2++;
                    if($this->points_p2 == 4 && !$this->post_deuce()){
                        $this->set_set($player);
                    }
                }
            }
        }
    }
    
    private function set_set($player){
        if(1 == $player){
            $this->sets_p1++;
        }
        else {
            $this->sets_p2++;
        }
        $this->points_p1 = $this->points[0];
        $this->points_p2 = $this->points[0];
    }
    
    public function deuce(){
        if(3 == $this->points_p1 && 3 == $this->points_p2){
            return true;
        }
        return false;
    }
    
    private function post_deuce(){
        if((3 == $this->points_p1 && 4 == $this->points_p2) ||
           (3 == $this->points_p2 && 4 == $this->points_p1)){
            return true;
        }
        return false;
    }
    
    public function is_new_set(){
        if(0 == $this->points_p1 && 0 == $this->points_p2){
            return true;
        }
        return false;
    }
    
    public function final_match(){
        if($this->max_sets == $this->sets_p1 || $this->max_sets == $this->sets_p2){
            return true;
        }
        return false;
    }
}
